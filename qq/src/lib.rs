extern crate proc_macro;

use proc_macro::TokenStream;
use quote::{quote, ToTokens};
use syn::{
  braced, bracketed,
  parse::{Parse, ParseStream, Result},
  parse_macro_input,
  punctuated::Punctuated,
  token::{Brace, Bracket},
  Expr, Path, Token,
};

struct KV(Expr, Val);

enum Val {
  Scalar(Box<Scalar>),
  List(List),
  Pairs(Pairs),
}

struct Scalar(Expr);
struct List(Punctuated<Val, Token![,]>);
struct Pairs(Punctuated<KV, Token![,]>);

impl Parse for KV {
  fn parse(input: ParseStream) -> Result<Self> {
    let k = input.parse()?;
    input.parse::<Token![=>]>()?;
    Ok(KV(k, input.parse()?))
  }
}

impl Parse for Scalar {
  fn parse(input: ParseStream) -> Result<Self> {
    Ok(Scalar(input.parse()?))
  }
}

impl ToTokens for Scalar {
  fn to_tokens(&self, tokens: &mut proc_macro2::TokenStream) {
    let v = &self.0;
    tokens.extend(quote! { String::from(#v) })
  }
}

impl Parse for List {
  fn parse(input: ParseStream) -> Result<Self> {
    let content;
    bracketed!(content in input);
    Ok(List(content.parse_terminated(Parse::parse)?))
  }
}

impl ToTokens for List {
  fn to_tokens(&self, tokens: &mut proc_macro2::TokenStream) {
    let l = &self.0;
    tokens.extend(quote! { ::std::vec![ #l ] })
  }
}

impl Parse for Pairs {
  fn parse(input: ParseStream) -> Result<Self> {
    let content;
    braced!(content in input);
    Ok(Pairs(content.parse_terminated(Parse::parse)?))
  }
}

impl ToTokens for Pairs {
  fn to_tokens(&self, tokens: &mut proc_macro2::TokenStream) {
    let kv_iter = self
      .0
      .iter()
      .map(|KV(k, v)| quote! { h.insert(String::from(#k), #v); });
    tokens.extend(quote! {
      {
        let mut h = ::std::collections::HashMap::<String, __Val>::new();
        #(#kv_iter)*
        h
      }
    })
  }
}

impl Parse for Val {
  fn parse(input: ParseStream) -> Result<Self> {
    let look = input.lookahead1();
    if look.peek(Brace) {
      Ok(Self::Pairs(input.parse()?))
    } else if look.peek(Bracket) {
      Ok(Self::List(input.parse()?))
    } else {
      Ok(Self::Scalar(input.parse()?))
    }
  }
}

impl ToTokens for Val {
  fn to_tokens(&self, tokens: &mut proc_macro2::TokenStream) {
    match self {
      Self::Pairs(p) => tokens.extend(quote! { __Val::Map(__Query::from(#p)) }),
      Self::List(l) => tokens.extend(quote! { __Val::List(#l) }),
      Self::Scalar(s) => tokens.extend(quote! { __Val::Scalar(#s) }),
    }
  }
}

struct WithPath<T>(Path, T);

impl<T: Parse> Parse for WithPath<T> {
  fn parse(input: ParseStream) -> Result<Self> {
    Ok(WithPath(input.parse()?, input.parse()?))
  }
}

#[proc_macro]
pub fn __qq(tokens: TokenStream) -> TokenStream {
  let WithPath(n, toks) = parse_macro_input!(tokens as WithPath<Pairs>);

  let body = quote! {
    {
      use #n::{Val as __Val, Query as __Query};
      #n::Query::from(#toks)
    }
  };

  TokenStream::from(body)
}
