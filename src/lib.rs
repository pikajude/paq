#![feature(proc_macro_hygiene)]

//! The URI builder in the `http` crate only provides a means to set the path and query
//! on a URI based on a `String`. This crate provides some combinators and parsing
//! utilities to convert values to and from query strings, and to iterate over them.

pub mod complex;
pub mod simple;

#[doc(hidden)]
pub use qq::__qq;

/// Shorthand macro to construct a nested [`Query`](complex::Query).
///
/// Inside `qq!`, values may be:
///
/// * A key/value map: `{ key => value, key2 => value2, ... }`. Keys follow the same rules
///   as scalars.
/// * A list with syntax `[ val1, val2, ... ]`.
/// * A scalar, which can be any valid Rust expression, including macro invocations.
///   It must evaluate to a type that implements `Into<String>`.
///
/// In order to embed a block or slice literal as a scalar, surround it with parentheses.
///
/// # Example
///
/// ```
/// // enable proc macros as expressions
/// #![feature(proc_macro_hygiene)]
///
/// let query = paq::qq!({
///   "str_key" => "str_value",
///   "list_key" => [
///     "list_item1",
///     "list_item2",
///   ],
///   "dict_key" => {
///     "nested_key1" => "foo",
///   },
///   // parenthesize the RHS for it to be interpreted as a block. otherwise
///   // it would be treated as a key/value map and fail to parse
///   "block_value" => ({
///     let n = 69 + 420;
///     format!("{}", n)
///   })
/// });
/// ```
#[macro_export]
macro_rules! qq {
  ( $($k:tt)+ ) => { $crate::__qq!($crate::complex $($k)+) }
}

/// Shorthand macro to construct a simple key/value query string.
///
/// ```
/// let query = paq::q!{
///   "foo" => "bar",
///   "baz" => "qux"
/// };
/// ```
#[macro_export]
macro_rules! q {
  ( $($k:expr => $v:expr),* ) => {
    {
      let mut h = std::collections::HashMap::<String,String>::new();
      $(h.insert(String::from($k), String::from($v));)*
      $crate::simple::Query::from(h)
    }
  }
}
