use std::fmt;
use std::iter::FromIterator;
use std::ops::{Deref, DerefMut};

/// A key path appearing in a query string.
///
/// `foo[][bar][1][...]=baz`
///
/// corresponds to
///
/// `Key("foo", ["", "bar", "1", "..."])`.
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord)]
pub struct Key<'a> {
  pub(super) top: &'a str,
  pub(super) path: Vec<&'a str>,
}

impl<'a> Key<'a> {
  pub fn new(s: &'a str) -> Self {
    Self {
      top: s,
      path: vec![],
    }
  }

  pub fn push(&self, s: &'a str) -> Self {
    let mut s2 = self.clone();
    s2.path.push(s);
    s2
  }
}

/// Newtype defined to allow failable `collect()` from an Iterator over string slices.
#[derive(Clone, Debug, Eq, PartialEq, Ord, PartialOrd)]
pub struct TryKey<'a>(Result<Key<'a>, EmptyKeyPath>);

/// This error is yielded when [`TryKey::from_iter`](TryKey) is run on an empty iterator.
#[derive(Clone, Debug, Eq, PartialEq, Ord, PartialOrd)]
pub struct EmptyKeyPath;

impl fmt::Display for EmptyKeyPath {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    write!(f, "Cannot create a Key without at least one element")
  }
}

impl std::error::Error for EmptyKeyPath {}

impl<'a> FromIterator<&'a str> for TryKey<'a> {
  fn from_iter<I: IntoIterator<Item = &'a str>>(it: I) -> Self {
    let mut iter = it.into_iter();
    Self(if let Some(n) = iter.next() {
      Ok(Key {
        top: n,
        path: iter.collect(),
      })
    } else {
      Err(EmptyKeyPath)
    })
  }
}

impl<'a> Deref for TryKey<'a> {
  type Target = Result<Key<'a>, EmptyKeyPath>;

  fn deref(&self) -> &Self::Target {
    &self.0
  }
}

impl<'a> DerefMut for TryKey<'a> {
  fn deref_mut(&mut self) -> &mut Self::Target {
    &mut self.0
  }
}
