use super::key::Key;
use super::Val;
use std::collections::VecDeque;

/// For each scalar appearing in a [`Query`](super::Query), this iterator yields
/// the full key path and value.
pub struct QueryIter<'a> {
  pub(super) nodes: VecDeque<(Key<'a>, &'a Val)>,
}

impl<'a> QueryIter<'a> {
  pub(crate) fn new<I: IntoIterator<Item = (Key<'a>, &'a Val)>>(it: I) -> Self {
    Self {
      nodes: it.into_iter().collect(),
    }
  }
}

impl<'a> Iterator for QueryIter<'a> {
  type Item = (Key<'a>, &'a String);

  fn next(&mut self) -> Option<Self::Item> {
    if let Some((k, node)) = self.nodes.pop_front() {
      match node {
        Val::Scalar(s) => return Some((k, s)),
        Val::List(ls) => self.nodes.extend(ls.iter().map(|l| (k.push(""), l))),
        Val::Map(m) => self.nodes.extend(m.0.iter().map(|(k2, v)| (k.push(k2), v))),
      }
      return self.next();
    }
    None
  }
}
