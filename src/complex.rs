//! Complex query strings, as an arbitrarily nested tree.
//!
//! Query strings in URLs can by represented by an arbitrarily deeply nested tree. For
//! example, the string `my_key[foo][][bar]=1` roughly translates to this object (in
//! JSON notation).
//!
//! ``` json
//! {
//!   my_key: {
//!     foo: [
//!       { bar: 1 }
//!     ]
//!   }
//! }
//! ```
//!
//! This module can be used to construct and manipulate such values.

mod iter;
mod key;
pub use iter::QueryIter;
pub use key::{EmptyKeyPath, Key, TryKey};

use bytes::{BufMut, BytesMut};
use http::uri::PathAndQuery;
use std::borrow::Borrow;
use std::collections::HashMap;
use std::iter::FromIterator;

/// A tree structure representing values that can appear in a query string.
///
/// Though you can use the enum constructors here directly, it's probably easier to use
/// [`qq!`](qq!).
#[derive(Debug, Eq, PartialEq, Clone)]
pub enum Val {
  /// `key=value1`
  Scalar(String),
  /// `key[]=value1&key[]=value2`
  List(Vec<Val>),
  /// `key[subkey]=value1&key[subkey2]=value2`
  Map(Query),
}

macro_rules! from {
  ( $t:ty, $t2:ty, $con:ident ) => {
    impl From<$t> for Val {
      fn from(t: $t) -> Self {
        Self::$con(t)
      }
    }

    impl FromIterator<$t2> for Val {
      fn from_iter<T: IntoIterator<Item = $t2>>(iter: T) -> Self {
        Self::$con(FromIterator::from_iter(iter))
      }
    }
  };
}

impl FromIterator<(String, Val)> for Query {
  fn from_iter<T: IntoIterator<Item = (String, Val)>>(iter: T) -> Self {
    Self(FromIterator::from_iter(iter))
  }
}

from!(String, char, Scalar);
from!(Vec<Val>, Val, List);
from!(Query, (String, Val), Map);

/// A set of key/value pairs. The [`qq!`](qq!) macro returns an instance of this.
///
/// ```
/// # #![feature(proc_macro_hygiene)]
/// let url = http::Uri::builder()
///   .scheme("https")
///   .authority("google.com")
///   .path_and_query(paq::qq!({
///     "key" => [ "value1", format!("{}", 42) ]
///   }).encode())
///   .build()
///   .unwrap();
/// assert_eq!(url, "https://google.com?key[]=value1&key[]=42");
/// ```
#[derive(Debug, Eq, PartialEq, Clone)]
pub struct Query(pub(crate) HashMap<String, Val>);

impl Query {
  /// Produces an iterator that yields the flattened key/value pairs of this query.
  #[allow(clippy::needless_lifetimes)] // false positive
  pub fn iter<'a>(&'a self) -> QueryIter<'a> {
    QueryIter::new(self.0.iter().map(|(k, n)| (key::Key::new(k), n)))
  }

  /// ```
  /// # #![feature(proc_macro_hygiene)]
  /// assert_eq!(
  ///   paq::qq!({
  ///     "foo" => "bar"
  ///   }).encode().into_bytes().as_ref(),
  ///   b"?foo=bar",
  /// );
  ///
  /// assert_eq!(
  ///   paq::qq!({
  ///     "🎂" => [ "yummy" ]
  ///   }).encode().into_bytes().as_ref(),
  ///   b"?%F0%9F%8E%82[]=yummy",
  /// );
  /// ```
  pub fn encode(&self) -> PathAndQuery {
    let mut buf = BytesMut::new();
    for (i, (k, v)) in self.iter().enumerate() {
      if i == 0 {
        buf.put(b'?');
      } else {
        buf.put(b'&');
      }
      Self::escape(k.top, &mut buf);
      for p in k.path {
        buf.put(b'[');
        Self::escape(p, &mut buf);
        buf.put(b']');
      }
      buf.put(b'=');
      Self::escape(v.as_str(), &mut buf);
    }
    PathAndQuery::from_shared(buf.freeze()).unwrap()
  }

  fn escape<S: Borrow<str>>(s: S, b: &mut BytesMut) {
    for byte in s.borrow().as_bytes() {
      match byte {
        b'a'..=b'z' | b'A'..=b'Z' | b'0'..=b'9' | b'*' | b'-' | b'.' | b'_' => b.put(*byte),
        b' ' => b.put(b'+'),
        x => b.extend(format!("%{:X}", x).as_bytes()),
      }
    }
  }
}

impl From<&'_ crate::simple::Query> for Query {
  fn from(q: &'_ crate::simple::Query) -> Self {
    Self::from_iter(q.0.iter().map(|(k, v)| (k.clone(), Val::Scalar(v.clone()))))
  }
}

impl From<HashMap<String, Val>> for Query {
  fn from(s: HashMap<String, Val>) -> Self {
    Self(s)
  }
}

impl<'a> IntoIterator for &'a Query {
  type Item = (Key<'a>, &'a String);
  type IntoIter = QueryIter<'a>;

  fn into_iter(self) -> Self::IntoIter {
    self.iter()
  }
}
