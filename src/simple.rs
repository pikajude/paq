//! Simple query strings, as a set of `(String, String)` key-value pairs.
//!
//! If you just want to fetch a single parameter from a URL's query string, this module
//! does what you need.
//!
//! Parsing a query string using this module will fail if it has complex keys like
//! `foo[]=bar`, because the characters `[]` are not allowed to appear unescaped
//! in simple keys.

use crate::complex;
use http::uri::PathAndQuery;
use std::collections::hash_map::HashMap;
use std::iter::FromIterator;

/// A struct representing a simple (string, string) key/value set.
///
/// Parsing for this type is faster than parsing the Complex variant.
///
/// Unlike `qq!`, [`q!`](q!) may be used without extra delimiters, since it only
/// supports scalar values:
///
/// ```
/// /*
/// this is a parse error:
/// let query = paq::qq!{
///   "key" => "value",
///   "key2" => "value2",
/// };
/// */
///
/// let query = paq::q!{
///   "key" => "value",
///   "key2" => "value2"
/// };
/// ```
#[derive(Debug, Eq, PartialEq, Clone)]
pub struct Query(pub(super) HashMap<String, String>);

impl Query {
  /// See [`encode`](crate::complex::Query::encode).
  ///
  /// ```
  /// # #![feature(proc_macro_hygiene)]
  /// assert_eq!(
  ///   paq::q!{"foo" => "bar"}
  ///     .encode().into_bytes().as_ref(),
  ///   b"?foo=bar",
  /// );
  /// ```
  pub fn encode(&self) -> PathAndQuery {
    complex::Query::from(self).encode()
  }
}

impl From<HashMap<String, String>> for Query {
  fn from(h: HashMap<String, String>) -> Self {
    Self(h)
  }
}

impl FromIterator<(String, String)> for Query {
  fn from_iter<T: IntoIterator<Item = (String, String)>>(iter: T) -> Self {
    Self(HashMap::from_iter(iter))
  }
}
